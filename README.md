# Accounts Payable Transmitted List Report

## Overview

The Transmitted Report is ran daily by the Accounts Payable team to track invoices
that are stuck in "transmitted" status. This project automates the process of
downloading the report from InvoiceWorks and moving it to the company file
server. The included Spotfire analysis uses this data to show what Accounts
Payable crewmembers need to work the "transmitted" invoices.

The project contact for Accounts Payble is [Alex Garcia](Sergio.Herrera@jetblue.com).

### Milestones

- Automatically download invoices in "transmitted" status from InvoiceWorks.
- Build Spotfire report to visualize data.

## Getting started

### Prerequisites

- Python 3.7 (with pip)
- Selenium IE Webdriver added to your user's PATH

### Installation

```bash
# Clone the repository
$ git clone git@bitbucket.org:jetbluefbi/accounts-payable-transmitted-list-report.git
$ cd accounts-payable-transmitted-list-report
# Install package locally
$ pip install .
```

### Usage

```bash
# Run the CLI
$ transmitted-list
```
