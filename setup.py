from setuptools import setup

setup(
    name='iwtransmittedlist',
    version='2018-09-18',
    install_requires=['requests', 'selenium'],
    author='Timothy Pettingill',
    author_email='timothy.pettingill@jetblue.com',
    packages=['iwtransmittedlist'],
    entry_points={
        'console_scripts': [
            'iw-transmitted-list = iwtransmittedlist.main:main'
        ]
    }
)