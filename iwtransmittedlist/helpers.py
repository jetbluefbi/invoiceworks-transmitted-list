from tkinter import TK
from tkinter.filedialog import askdirectory, askopenfilename, asksaveasfilename
from pathlib import Path



def prompt_directory(show_dialog=True, **kwargs):
    if show_dialog:
        root = Tk()
        filepath = askdirectory(**kwargs)
        root.destroy()
    else:
        filepath = input('Path to directory: ')
    filepath = Path(filepath).resolve()
    return filepath

def prompt_input_filepath(show_dialog=True):
    if show_dialog:
        root = Tk()
        filepath = askopenfilename(**kwargs)
        root.destroy()
    else:
        filepath = input('Path to input file: ')
    filepath = Path(filepath).resolve()
    return filepath

def prompt_output_filepath(show_dialog=True):
    if show_dialog:
        root = Tk()
        filepath = asksaveasfilename(**kwargs)
        root.destroy()
    else:
        filepath = input('Path to output file: ')
    filepath = Path(filepath).resolve()
    if not all([x.exists() and x.is_dir() for x in filepath.parents]):
        filepath.parent.mkdir(parents=True, exist_ok=True)
    return filepath