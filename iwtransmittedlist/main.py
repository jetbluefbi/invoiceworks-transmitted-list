from pathlib import Path

from .iwbots import IWArchiveBot


REPORT_NAME = 'AP Transmitted List'
REPORT_ID = 194
SAVE_FILEPATH = '//sscdfs_new/SharedFile/payments/accounts-payable/transmitted-list/ap-transmitted-list.csv'


# TODO (Tyler): Implement same functionality as the match-exception bot to download report and save to SAVE_FILEPATH
def main():
    # get report
    abot = IWArchiveBot()
    abot.connect()
    content = abot.export_report_as_csv(REPORT_ID)
    # save report
    fp = Path(SAVE_FILEPATH).resolve()
    # open fp and write content to it!
    # f = open(fp , 'wb')
    # f.write(content)
    # f.close()

    #using with statement
    # with open(fp) as f:
    #     write_data = f.write()
    #using pathlib library function
    p = Path(fp)
    p.write_bytes(fp)
    

if __name__ == '__main__':
    main()
    
    
    